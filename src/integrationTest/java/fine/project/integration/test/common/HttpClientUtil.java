package fine.project.integration.test.common;


import fine.project.model.User;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.UUID;


public class HttpClientUtil {

    private static final String TARGET = "http://localhost:8090/tasksharing/api";

    private static ResponseErrorHandler responseErrorHandler = new ResponseErrorHandler() {
        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            return false;
        }

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
        }
    };

    private HttpClientUtil() {
    }

    public static RestTemplate restTemplateMuteException() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(responseErrorHandler);
        return restTemplate;
    }

    public static RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void addBasicAuth(RestTemplate restTemplate, String usernName, String password) {
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(usernName, password));
    }

    public static RestTemplate getAuthenticatedRestTemplate() {
        RestTemplate restTemplate = restTemplate();
        String userName = UUID.randomUUID().toString();
        String password = UUID.randomUUID().toString();
        restTemplate.postForEntity(
                AppLinks.USER_REGISTRATION, User.builder().loginName(userName).password(password)
                        .build(), User.class);
        addBasicAuth(restTemplate, userName, password);
        return restTemplate;
    }
}
