package fine.project.integration.test.common;


public class AppLinks {
    private AppLinks(){}

    public static String BASE ="http://localhost:8090/tasksharing/api";
    public static String USER = BASE +"/user";
    public static String USER_REGISTRATION = USER +"/register";
    public static String MESSAGES = BASE +"/messages";
    public static String MESSAGES_ID = BASE +"/messages/{id}";
    public static String TASKS = BASE +"/tasks/";
    public static String TASKS_ID = TASKS +"{id}";
}
