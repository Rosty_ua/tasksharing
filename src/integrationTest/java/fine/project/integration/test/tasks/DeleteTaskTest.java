package fine.project.integration.test.tasks;

import fine.project.model.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeleteTaskTest {
    private TaskWebService taskWebService = new TaskWebService();
    private Task createdTask = null;

    @Before
    public void setUp() {
        createdTask = taskWebService.createDefTask();
    }

    @Test
    public void deleteTaskTest() {
        Task task = taskWebService.deleteTaskById(createdTask.getId());
        Assert.assertEquals(createdTask, task);
        Task taskById = taskWebService.getTaskById(createdTask.getId());
        Assert.assertNull(taskById);
    }
}
