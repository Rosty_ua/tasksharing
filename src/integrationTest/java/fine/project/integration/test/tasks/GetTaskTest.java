package fine.project.integration.test.tasks;

import fine.project.model.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rostyslav.Pash on 19-Apr-17.
 */
public class GetTaskTest {
    private TaskWebService taskWebService = new TaskWebService();
    private List<Task> createdTask = new ArrayList<>();

    @Before
    public void setUp() {
        createdTask.add(taskWebService.createDefTask());
        createdTask.add(taskWebService.createDefTask());
        createdTask.add(taskWebService.createDefTask());
        createdTask.add(taskWebService.createDefTask());
    }

    @Test
    public void getAllTasksTest() {
        List<Task> allTasks = taskWebService.getAllTasks();
        Assert.assertNotNull(allTasks);
        Assert.assertFalse(allTasks.isEmpty());
        Assert.assertTrue(allTasks.containsAll(createdTask));
    }

    @Test
    public void getTaskTest() {
        Task task = createdTask.get(1);
        int id = task.getId();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskWebService.getTaskById(id), task);
    }
}
