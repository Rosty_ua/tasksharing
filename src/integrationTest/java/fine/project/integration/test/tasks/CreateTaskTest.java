package fine.project.integration.test.tasks;

import fine.project.model.Task;
import org.junit.Assert;
import org.junit.Test;

public class CreateTaskTest {
    TaskWebService taskWebService = new TaskWebService();

    @Test
    public void createTaskTest() {
        Task createdTask = taskWebService.createDefTask();
        Task task = taskWebService.getTaskById(createdTask.getId());
        Assert.assertEquals(createdTask, task);
    }
}
