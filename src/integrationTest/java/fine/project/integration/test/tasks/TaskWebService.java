package fine.project.integration.test.tasks;

import fine.project.integration.test.common.AppLinks;
import fine.project.integration.test.common.HttpClientUtil;
import fine.project.model.Status;
import fine.project.model.Task;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class TaskWebService {
    private RestTemplate restTemplate = HttpClientUtil.getAuthenticatedRestTemplate();

    public Task createDefTask() {
        Task task;
        task = Task.builder().message("Test message").status(Status.OPEN).build();
        return restTemplate.postForEntity(AppLinks.TASKS, task, Task.class).getBody();
    }

    public List<Task> getAllTasks() {
        ResponseEntity<List<Task>> taskList = restTemplate.exchange(AppLinks.TASKS, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Task>>() {
                });
        return taskList.getBody();
    }

    public Task getTaskById(int id) {
        return restTemplate.getForEntity(AppLinks.TASKS_ID, Task.class, id).getBody();
    }

    public Task deleteTaskById(int id) {
        return restTemplate.exchange(AppLinks.TASKS_ID, HttpMethod.DELETE, null, Task.class, id).getBody();
    }

    public Task updateTask(int id, Task task) {
        return restTemplate.exchange(AppLinks.TASKS_ID, HttpMethod.PUT, new HttpEntity<>(task),
                Task.class, id).getBody();
    }

}
