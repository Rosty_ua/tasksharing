package fine.project.integration.test.tasks;

import fine.project.model.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UpdateTaskTest {
    private TaskWebService taskWebService= new TaskWebService();
    private Task createdTask;

    @Before
    public void setUp() {
        createdTask = taskWebService.createDefTask();
    }

    @Test
    public void updateTaskTest() {
        createdTask.setMessage("new test message!!!");
        Task updatedTask = taskWebService.updateTask(createdTask.getId(), createdTask);
        Assert.assertEquals(createdTask.getMessage(), updatedTask.getMessage());
        Task taskById = taskWebService.getTaskById(createdTask.getId());
        Assert.assertEquals(updatedTask, taskById);
    }
}
