package fine.project.integration.test.auth;

import fine.project.integration.test.common.AppLinks;
import fine.project.integration.test.common.HttpClientUtil;
import fine.project.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

public class AuthenticationTest {
    @Test
    public void nonAuthorizedTest() {
        RestTemplate restTemplate = HttpClientUtil.restTemplateMuteException();
        ResponseEntity<String> userResponseEntity = restTemplate.getForEntity(AppLinks.USER, String.class);
        Assert.assertEquals(HttpStatus.UNAUTHORIZED, userResponseEntity.getStatusCode());
    }

    @Test
    public void authorizedTest() {

        RestTemplate restTemplate = HttpClientUtil.restTemplateMuteException();

        String username = UUID.randomUUID().toString();
        String pass = UUID.randomUUID().toString();

        HttpClientUtil.addBasicAuth(restTemplate, username, pass);

        ResponseEntity<String> userResponseEntityUnauthorized = restTemplate.getForEntity(AppLinks.USER, String.class);
        Assert.assertEquals(HttpStatus.UNAUTHORIZED, userResponseEntityUnauthorized.getStatusCode());

        User user = User.builder().loginName(username).password(pass).build();
        ResponseEntity<User> registeredUser = restTemplate.postForEntity(AppLinks.USER_REGISTRATION, user, User
                .class);

        Assert.assertEquals(HttpStatus.CREATED, registeredUser.getStatusCode());

        ResponseEntity<User> userResponseEntity = restTemplate.getForEntity(AppLinks.USER, User.class);
        Assert.assertEquals(HttpStatus.OK, userResponseEntity.getStatusCode());

        User authUser = userResponseEntity.getBody();
        Assert.assertEquals(username, authUser.getLoginName());
    }

    @Test
    public void duplicateUserAuthorizedTest() {
        RestTemplate restTemplate = HttpClientUtil.restTemplateMuteException();

        String username = UUID.randomUUID().toString();
        String pass = UUID.randomUUID().toString();

        User user = User.builder().loginName(username).password(pass).build();
        ResponseEntity<User> registeredUser = restTemplate.postForEntity(AppLinks.USER_REGISTRATION, user, User
                .class);

        Assert.assertEquals(HttpStatus.CREATED, registeredUser.getStatusCode());

        ResponseEntity<String> registeredUserDuplicate = restTemplate.postForEntity(AppLinks.USER_REGISTRATION, user,
                String
                .class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, registeredUserDuplicate.getStatusCode());
    }

}
