package fine.project.integration.test.message;


import fine.project.model.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UpdateMessageTest {
        MessageWebService messageWebService = new MessageWebService();

    Message createdMessage;

    @Before
    public void setUp() throws Exception {
        createdMessage = messageWebService.createDefMessage();
    }

    @Test
    public void updateMessageTest() {
        createdMessage.setAuthor("newAuthor");
        Message updatedMessage = messageWebService.updateMessage(createdMessage);
        Assert.assertEquals(createdMessage.getAuthor(),updatedMessage.getAuthor());
        Message message = messageWebService.getMessageById(createdMessage.getId());
        Assert.assertEquals(updatedMessage, message);
    }

}
