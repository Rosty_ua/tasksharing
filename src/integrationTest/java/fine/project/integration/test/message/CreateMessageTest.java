package fine.project.integration.test.message;


import fine.project.model.Message;
import org.junit.Assert;
import org.junit.Test;

public class CreateMessageTest {
    MessageWebService messageWebService = new MessageWebService();

    @Test
    public void createMessageTest() {
        Message createdMessage = messageWebService.createDefMessage();
        Message message = messageWebService.getMessageById(createdMessage.getId());
        Assert.assertEquals(createdMessage, message);
    }

}
