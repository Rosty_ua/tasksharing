package fine.project.integration.test.message;

import fine.project.model.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GetAllMessagesTest {

    MessageWebService messageWebService = new MessageWebService();
    Set<Message> createdMessages = new HashSet<>();

    @Before
    public void setUp() throws Exception {
        createdMessages.add(messageWebService.createDefMessage());
        createdMessages.add(messageWebService.createDefMessage());
        createdMessages.add(messageWebService.createDefMessage());
        createdMessages.add(messageWebService.createDefMessage());
    }

    @Test
    public void getAllMessagesTest() {
        List<Message> allMessages = messageWebService.getAllMessages();
        Assert.assertNotNull(allMessages);
        Assert.assertFalse(allMessages.isEmpty());
        Assert.assertTrue(allMessages.containsAll(createdMessages));
    }
}
