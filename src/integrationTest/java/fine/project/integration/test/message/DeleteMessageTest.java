package fine.project.integration.test.message;


import fine.project.model.Message;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DeleteMessageTest {
    MessageWebService messageWebService = new MessageWebService();

    Message createdMessage = null;
    @Before
    public void setUp() throws Exception {
        createdMessage = messageWebService.createDefMessage();
    }

    @Test
    public void deleteMessageByIdTest(){
        Message message = messageWebService.deleteMessageById(createdMessage.getId());
        Assert.assertEquals(createdMessage,message);
        Message messageById = messageWebService.getMessageById(createdMessage.getId());
        Assert.assertNull(messageById);
    }


}
