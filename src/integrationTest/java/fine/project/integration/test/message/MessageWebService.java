package fine.project.integration.test.message;

import fine.project.integration.test.common.AppLinks;
import fine.project.integration.test.common.HttpClientUtil;
import fine.project.model.Message;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;


public class MessageWebService {
    private RestTemplate restTemplate = HttpClientUtil.getAuthenticatedRestTemplate();

    public Message createDefMessage() {
        Message message = Message.builder().author("Author").msg("Message").build();
        return restTemplate.postForEntity(AppLinks.MESSAGES, message, Message.class).getBody();
    }

    public List<Message> getAllMessages() {
        ResponseEntity<List<Message>> messageList = restTemplate.exchange(AppLinks.MESSAGES, HttpMethod.GET, null, new
                ParameterizedTypeReference<List<Message>>() {
                });
        return messageList.getBody();
    }

    public Message getMessageById(int id) {
        return restTemplate.getForEntity(AppLinks.MESSAGES_ID, Message.class, id).getBody();
    }

    public Message deleteMessageById(int id) {
        return restTemplate.exchange(AppLinks.MESSAGES_ID, HttpMethod.DELETE, null, Message.class, id).getBody();
    }

    Message updateMessage(Message message) {
        return restTemplate.exchange(AppLinks.MESSAGES, HttpMethod.PUT, new HttpEntity<>(message), Message
                .class).getBody();
    }
}
