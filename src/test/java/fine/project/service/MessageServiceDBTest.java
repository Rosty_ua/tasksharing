package fine.project.service;

import fine.project.model.Message;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring/applicationContext.xml")
public class MessageServiceDBTest {
    @Autowired
    MessageService messageService;

    @Test
    public void createMessageTest() throws Exception {

        // Create 10 messages
        List<Message> createdMessages = IntStream.range(0, 10).mapToObj((i) -> messageService.addMessage(new Message()))
                .collect(Collectors.toList());

        List<Message> allMessages = messageService.getAllMessages();

        Assert.assertNotNull(allMessages);
        Assert.assertNotEquals(0,allMessages.size());
        Assert.assertTrue(allMessages.containsAll(createdMessages));
    }
}
