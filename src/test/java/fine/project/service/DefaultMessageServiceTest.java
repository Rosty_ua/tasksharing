package fine.project.service;

import java.util.ArrayList;
import java.util.List;

import fine.project.dao.CrudDao;
import fine.project.dao.impl.MessageDao;
import fine.project.service.impl.DefaultMessageService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import fine.project.model.Message;
import org.junit.Assert;

import static org.mockito.MockitoAnnotations.initMocks;

public class DefaultMessageServiceTest {

    @Mock
    MessageDao messageDao;

    @InjectMocks
    DefaultMessageService defaultMessageService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetAllMessages() {
        List<Message> list = new ArrayList<>();
        Mockito.when(messageDao.getAll()).thenReturn(list);
        Assert.assertEquals(list, defaultMessageService.getAllMessages());
    }

}
