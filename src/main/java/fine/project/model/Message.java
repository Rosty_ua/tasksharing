package fine.project.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@Builder
@XmlRootElement
@Entity(name = "Messages")
public class Message {

    @Tolerate
    public Message() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String msg;
    private Long created;
    private Long modified;
    private String author;
}
