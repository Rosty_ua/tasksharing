package fine.project.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Data
@Builder
@Entity(name = "Comments")
public class Comment {

    @Tolerate
    public Comment() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer commentId;
    private Long date;
    private Long modified;
    private String message;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "taskId")
    private Task commentOwner;

}
