package fine.project.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@Builder
@XmlRootElement
@Entity(name = "Users")
public class User implements Serializable {
    @Id
    @Column(name = "loginName", unique = true, nullable = false)
    private String loginName;
    private String password;
    private String name;
    private String surname;
    private String phone;
    private String email;

    @Tolerate
    public User() {
    }
}

