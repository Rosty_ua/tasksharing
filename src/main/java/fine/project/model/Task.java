package fine.project.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Data
@Builder
@Entity(name = "Tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String message;
    private Long createdDate;
    private Long modifiedDate;
    @Enumerated(EnumType.STRING)
    private Status status;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private User owner;

    @Tolerate
    public Task() {
    }

}

