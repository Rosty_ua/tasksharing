package fine.project.model;


public enum Status {
    OPEN,
    DONE,
    REOPENED,
    POSTPONED,
    CANCELED
}
