package fine.project.dao.impl;

import fine.project.model.Task;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class TaskDao extends DefaultCrudDao<Task> {
    @PersistenceContext
    private EntityManager entityManager;

    public TaskDao() {
        super(Task.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
}
