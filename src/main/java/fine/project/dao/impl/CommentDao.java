package fine.project.dao.impl;

import fine.project.model.Comment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CommentDao extends DefaultCrudDao<Comment> {
    @PersistenceContext
    private EntityManager entityManager;

    public CommentDao() {
        super(Comment.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
}
