package fine.project.dao.impl;


import fine.project.dao.CrudDao;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;


public abstract class DefaultCrudDao<T> implements CrudDao<T> {

    private Class<T> clazz;

    public DefaultCrudDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T get(Serializable id) {
        return getEntityManager().find(clazz, id);
    }

    @Override
    public void delete(Serializable id) {
        T entity = get(id);
        delete(entity);
    }

    @Override
    public void delete(T entity) {
        if (!getEntityManager().contains(entity)) {
            getEntityManager().merge(entity);
        }
        getEntityManager().remove(entity);
    }

    @Override
    public List<T> getAll() {
        CriteriaQuery<T> query = getEntityManager().getCriteriaBuilder().createQuery(clazz);
        CriteriaQuery<T> selectAll = query.select(query.from(clazz));
        return getEntityManager().createQuery(selectAll).getResultList();
    }

    @Override
    public T update(T entity) {
        getEntityManager().merge(entity);
        return entity;
    }

    @Override
    public T create(final T entity) {
        getEntityManager().persist(entity);
        return entity;
    }

    protected abstract EntityManager getEntityManager();
}
