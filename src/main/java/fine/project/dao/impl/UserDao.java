package fine.project.dao.impl;

import fine.project.model.User;
import fine.project.model.User_;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDao extends DefaultCrudDao<User> {

    @PersistenceContext
    private EntityManager entityManager;

    public UserDao() {
        super(User.class);
    }

    public Optional<User> getUserByCredentials(String username, String password) {
        CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<User> criteria = criteriaBuilder.createQuery(User.class);
        Root<User> from = criteria.from(User.class);
        Predicate userNamePredicate = criteriaBuilder.equal(from.get(User_.loginName), username);
        Predicate passwordPredicate = criteriaBuilder.equal(from.get(User_.password), password);
        CriteriaQuery<User> credentialsPredicate = criteria.select(from).where(criteriaBuilder.and(userNamePredicate,
                passwordPredicate));
        List<User> users = getEntityManager().createQuery(credentialsPredicate).getResultList();
        if (users.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(users.get(0));
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
