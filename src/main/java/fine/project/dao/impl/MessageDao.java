package fine.project.dao.impl;

import fine.project.model.Message;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class MessageDao extends DefaultCrudDao<Message> {
    @PersistenceContext
    private EntityManager entityManager;

    public MessageDao() {
        super(Message.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
}
