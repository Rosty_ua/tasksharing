package fine.project.dao;


import java.io.Serializable;
import java.util.List;

public interface CrudDao<T> {
    T get(Serializable id);

    void delete(Serializable id);

    void delete(T entity);

    List<T> getAll();

    T update(T entity);

    T create(T entity);
}
