package fine.project.controller;

import fine.project.model.Comment;
import fine.project.service.CommentService;
import fine.project.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping(value = "/tasks/{taskId}/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private TaskService taskService;

    @GetMapping(value = "/{commentId}")
    public Comment getComment(@PathVariable("commentId") final int id) {
        return commentService.getComment(id);
    }

    @GetMapping
    public List<Comment> getComments(@PathVariable("taskId") final int taskId) {
        return commentService.getAllComments(taskId);
    }

    @DeleteMapping(value = "/{commentId}")
    public Comment deleteComment(@PathVariable("commentId") final int id) {
        return commentService.removeComment(id);
    }

    @PutMapping(value = "/{commentId}")
    public Comment updateComment(@PathVariable("commentId") final int commentId, @RequestBody Comment comment) {
        comment.setCommentId(commentId);
        return commentService.updateComment(comment);
    }

    @PostMapping
    public Comment createComment(@PathVariable("taskId") final int taskId, @RequestBody final Comment comment) {
        comment.setCommentOwner(taskService.getTask(taskId));
        return commentService.createComment(comment);
    }
}
