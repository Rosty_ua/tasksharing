package fine.project.controller;

import fine.project.model.Task;
import fine.project.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/tasks")
@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping(value = "/{taskId}")
    public Task getTask(@PathVariable("taskId") final int id) {
        return taskService.getTask(id);
    }

    @GetMapping
    public List<Task> getAllTask() {
        return taskService.getAllTask();
    }

    @DeleteMapping(value = "/{taskId}")
    public Task deleteTask(@PathVariable("taskId") final int id) {
        return taskService.removeTask(id);
    }

    @PutMapping(value = "/{taskId}")
    public Task updateTask(@PathVariable("taskId") final int id, @RequestBody final Task task) {
        return taskService.updateTask(id, task);
    }

    @PostMapping
    public Task addTask(@RequestBody final Task task) {
        return taskService.createTask(task);
    }

}
