package fine.project.controller;

import fine.project.model.User;
import fine.project.service.UserService;
import fine.project.service.exception.DuplicateUserException;
import fine.project.service.exception.UserNotFoundInContextException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public User currentUser() throws UserNotFoundInContextException {
        return userService.getCurrentUser();
    }

    @PostMapping(value = "/register")
    public ResponseEntity registerUser(@RequestBody final User user) {
        ResponseEntity response;
        try {
            User createdUser = userService.createUser(user);
            response = ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
        } catch (DuplicateUserException e) {
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        return response;
    }
}
