package fine.project.controller;

import fine.project.model.Message;
import fine.project.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@PreAuthorize("isAuthenticated()")
@RestController
@RequestMapping(value = "/messages")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PutMapping
    public Message updateMessage(@RequestBody final Message message) {
        return messageService.updateMessage(message);
    }

    @DeleteMapping(value = "/{messageId}")
    public Message deleteMessage(@PathVariable("messageId") final int id) {
        return messageService.removeMessage(id);
    }

    @PostMapping
    public Message addMessage(@RequestBody final Message message) {
        return messageService.addMessage(message);
    }

    @GetMapping
    public List<Message> getMessages() {
        return messageService.getAllMessages();
    }

    @GetMapping(value = "/{messageId}")
    public Message getMessage(@PathVariable("messageId") final int id) {
        return messageService.getMessage(id);
    }
}
