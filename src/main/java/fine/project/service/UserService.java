package fine.project.service;

import fine.project.model.User;
import fine.project.service.exception.DuplicateUserException;
import fine.project.service.exception.UserNotFoundInContextException;

import java.util.Optional;

public interface UserService {
    User createUser(User user) throws DuplicateUserException;

    Optional<User> findUserByCredentials(String userName, String password);

    User getCurrentUser() throws UserNotFoundInContextException;
}
