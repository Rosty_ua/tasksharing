package fine.project.service;

import fine.project.model.Comment;

import java.util.List;


public interface CommentService {
    Comment createComment(Comment comment);

    Comment updateComment(Comment comment);

    Comment removeComment(Integer id);

    Comment getComment(Integer id);

    List<Comment> getAllComments(Integer id);

}
