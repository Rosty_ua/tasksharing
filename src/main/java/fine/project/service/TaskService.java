package fine.project.service;

import fine.project.model.Task;

import java.util.List;


public interface TaskService {
    Task createTask(Task task);

    Task updateTask(Integer id, Task task);

    Task removeTask(Integer id);

    Task getTask(Integer id);

    List<Task> getAllTask();

}
