package fine.project.service.impl;

import fine.project.dao.impl.MessageDao;
import fine.project.model.Message;
import fine.project.service.MessageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DefaultMessageService implements MessageService {

    @Autowired
    private MessageDao messageDao;
    private static Logger log = LogManager.getLogger(DefaultMessageService.class);

    @Override
    public List<Message> getAllMessages() {
        log.info("Getting all messages");
        List<Message> list = messageDao.getAll();
        log.debug("Getting all messages: \n", () -> list);
        return list;
    }

    @Override
    @Transactional
    public Message addMessage(Message message) {
        log.info("Adding message");
        message.setCreated(System.currentTimeMillis());
        log.debug("Adding message: ", () -> message);
        return messageDao.create(message);
    }

    @Override
    @Transactional
    public Message updateMessage(Message message) {
        log.info("Updating message");
        message.setModified(System.currentTimeMillis());
        log.debug("Updating message: ", () -> message);
        return messageDao.update(message);
    }

    @Override
    @Transactional
    public Message removeMessage(Integer id) {
        log.info("Removing message");
        Message message = messageDao.get(id);
        messageDao.delete(message);
        log.debug("Removing message: ", () -> message);
        return message;
    }

    @Override
    public Message getMessage(Integer id) {
        log.info("Getting message");
        Message message = messageDao.get(id);
        log.debug("Getting message: ", () -> message);
        return message;
    }

}
