package fine.project.service.impl;

import fine.project.dao.impl.CommentDao;
import fine.project.dao.impl.TaskDao;
import fine.project.model.Comment;
import fine.project.service.CommentService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
@Transactional
public class DefaultCommentServiceImpl implements CommentService {
    @Autowired
    private CommentDao commentDao;
    @Autowired
    private TaskDao taskDao;

    @Override
    public Comment createComment(Comment comment) {
        log.info("Creating new comment for task");
        log.debug("Creating new comment for task " + comment);
        return commentDao.create(comment);
    }

    //@Transactional
    @Override
    public Comment updateComment(Comment comment) {
        log.info("Updating comment for task");
        if (comment.getCommentOwner() == null) {
            comment.setCommentOwner(taskDao.get(comment.getCommentId()));
        }
        log.debug("Updating comment " + comment);
        log.debug("For task " + comment.getCommentOwner());
        return commentDao.update(comment);
    }

    //@Transactional
    @Override
    public Comment removeComment(Integer id) {
        log.info("Removing comment");
        Comment comment = commentDao.get(id);
        log.debug("Removing comment " + comment);
        commentDao.delete(comment);
        return comment;
    }

    @Override
    public Comment getComment(Integer id) {
        log.info("Getting comment with id: " + id);
        Comment comment = commentDao.get(id);
        log.debug("Getting comment " + comment + " for task " + comment.getCommentOwner());
        return commentDao.get(id);
    }


    //@Transactional
    @Override
    public List<Comment> getAllComments(Integer taskId) {
        log.info("Getting all comments for task : " + taskId);
        List<Comment> comments = commentDao.getAll()
                .stream()
                .filter(comment -> comment.getCommentOwner().getId() == taskId)
                .collect(Collectors.toList());
        log.debug("Getting all comments for task : " + taskId);
        comments.forEach(s -> log.debug(s));
        return comments;
    }
}
