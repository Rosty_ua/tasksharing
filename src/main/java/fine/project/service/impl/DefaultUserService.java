package fine.project.service.impl;

import fine.project.dao.impl.UserDao;
import fine.project.model.User;
import fine.project.service.UserService;
import fine.project.service.exception.DuplicateUserException;
import fine.project.service.exception.UserNotFoundInContextException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class DefaultUserService implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public User createUser(User user) throws DuplicateUserException {
        if (userDao.get(user.getLoginName()) != null) {
            throw new DuplicateUserException("User with already exists with name " + user.getLoginName());
        }
        return userDao.create(user);
    }

    @Override
    public Optional<User> findUserByCredentials(String userName, String password) {
        return userDao.getUserByCredentials(userName, password);
    }

    @Override
    public User getCurrentUser() throws UserNotFoundInContextException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return (User) authentication.getPrincipal();
        }
        throw new UserNotFoundInContextException("Unable to find user in SecurityContext");
    }
}
