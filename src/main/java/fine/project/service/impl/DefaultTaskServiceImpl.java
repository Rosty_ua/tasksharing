package fine.project.service.impl;

import fine.project.dao.impl.CommentDao;
import fine.project.dao.impl.TaskDao;
import fine.project.model.Comment;
import fine.project.model.Status;
import fine.project.model.Task;
import fine.project.service.TaskService;
import fine.project.service.UserService;
import fine.project.service.exception.UserNotFoundInContextException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Transactional
@Service
public class DefaultTaskServiceImpl implements TaskService {
    @Autowired
    private UserService userService;
    @Autowired
    private TaskDao taskDao;
    @Autowired
    private CommentDao commentDao;

    @Override
    @Transactional
    public Task createTask(Task task) {
        log.info("Creating new task");
        log.debug("Creating new task " + task);
        try {
            task.setOwner(userService.getCurrentUser());
            task.setStatus(Status.OPEN);
        } catch (UserNotFoundInContextException e) {
            e.printStackTrace();
        }
        return taskDao.create(task);
    }

    //@Transactional
    @Override
    public Task updateTask(Integer id, Task task) {
        log.info("Updating task");
        log.debug("Updating task " + task);
        // ==============>   realization is shit, don't know how to optimize it :) will be in new branch
        if (task.getOwner() == null) {
            task.setOwner(taskDao.get(id).getOwner());
        }

        if (task.getId() == 0) {
            task.setId(id);
        }

        if (task.getCreatedDate() == null) {
            task.setCreatedDate(taskDao.get(id).getCreatedDate());
        }

        if (task.getModifiedDate() == null) {
            task.setModifiedDate(taskDao.get(id).getModifiedDate());
        }

        if (task.getModifiedDate() == null) {
            task.setModifiedDate(taskDao.get(id).getModifiedDate());
        }

        if (task.getMessage() == null) {
            task.setMessage(taskDao.get(id).getMessage());
        }

        if (task.getStatus() == null) {
            task.setStatus(taskDao.get(id).getStatus());
        }
        // <========================================================
        return taskDao.update(task);
    }

    //@Transactional
    @Override
    public Task removeTask(Integer id) {
        log.info("Removing task");
        Task task = taskDao.get(id);
        log.debug("Removing task " + task);
        log.info("Remove all comments for this current task");
        // ======> to be replaced with correct query selection by taskId !!!! will be in new branch
        for (Comment c : commentDao.getAll()) {
            if (c.getCommentOwner().getId() == id) {
                log.debug("Removing comment: " + c);
                commentDao.delete(c);
            }
        }
        // <=======
        taskDao.delete(task);
        return task;
    }

    @Override
    public Task getTask(Integer id) {
        log.info("Getting task");
        Task task = taskDao.get(id);
        log.debug("Getting task " + task);
        return task;
    }


    public List<Task> getAllTask() {
        log.info("Getting task");
        log.debug("Getting all tasks");
        return taskDao.getAll();
    }
}
