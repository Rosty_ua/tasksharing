package fine.project.service.exception;

public class UserNotFoundInContextException extends Exception {
    public UserNotFoundInContextException(String message) {
        super(message);
    }
}
