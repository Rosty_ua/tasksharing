package fine.project.service;

import java.util.List;

import fine.project.model.Message;

public interface MessageService {

    List<Message> getAllMessages();

    Message addMessage(Message message);

    Message updateMessage(Message message);

    Message removeMessage(Integer id);

    Message getMessage(Integer id);

}
