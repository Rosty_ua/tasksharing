package fine.project.auth;

import fine.project.dao.impl.UserDao;
import fine.project.model.User;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Log4j2
@Component(value = "authenticationProvider")
public class CustomAuthenticationProvider
        implements AuthenticationProvider {

    @Autowired
    private UserDao userDao;


    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        log.info("Trying to authenticate user {}", name);

        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(password)) {
            log.info("Authentication fails of user {}", name);
            return createAnonymousAuthentication();
        }

        Optional<User> userByCredentials = userDao.getUserByCredentials(name, password);
        if (userByCredentials.isPresent()) {
            log.info("Authentication success of user {}", name);
            return new UsernamePasswordAuthenticationToken(userByCredentials.get(), password, new ArrayList<>());
        } else {
            log.info("Authentication fails of user {}", name);
            return createAnonymousAuthentication();
        }
    }

    private AnonymousAuthenticationToken createAnonymousAuthentication() {
        AnonymousAuthenticationToken anonymousAuthenticationToken = new AnonymousAuthenticationToken("ANONYMOUS",
                "ANONYMOUS", AuthorityUtils.createAuthorityList("ANONYMOUS"));
        anonymousAuthenticationToken.setAuthenticated(false);
        return anonymousAuthenticationToken;
    }


    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
                UsernamePasswordAuthenticationToken.class);
    }
}
